
def greetings(name):
    print('Hello {}'.format(name))

if __name__ == '__main__':
    name = 'World'
    greetings(name)